module "resource_group" {
  source                    = "./modules/resource_group"
  prefix                    = var.prefix
  location                  = var.location
  resource_group_name       = var.resource_group_name
}

module "network" {
  source              = "./modules/network"
  resource_group_name = module.resource_group.name
  prefix              = var.prefix
  location            = var.location
  vnet_cidr           = var.vnet_cidr
  subnet_cidr         = var.subnet_cidr
}

#module "acr" {
#  source                 = "./modules/acr"
#  resource_group_name    = module.resource_group.name
#  acr_name               = var.acr_name
#  acr_sku                = var.acr_sku
#  location               = var.location
#}

module "aks" {
  source                  = "./modules/aks"
  resource_group_name     = module.resource_group.name
  prefix                  = var.prefix
  location                = var.location
  node_count              = var.node_count
  node_size               = var.node_size
  k8s_cluster_name        = var.k8s_cluster_name
  k8s_cluster_node_pool   = var .k8s_cluster_node_pool
  k8s_version             = var.k8s_version
  vnet_name               = module.network.vnet_name
  subnet_id               = module.network.subnet_id
  client_id               = var.client_id
  client_secret           = var.client_secret
}
