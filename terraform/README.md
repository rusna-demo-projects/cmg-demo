# Terraform

To be able to run terraform you need to have account in Azure. 
If you want to run terraform apply from localhost you need to export your `client_id` and `client_secret` like that.

You can find out how to create service principal on this page: 
https://learn.microsoft.com/en-us/azure/developer/terraform/authenticate-to-azure?tabs=bash#create-a-service-principal

If you want to run terraform from localhost you need to export these variables:

```
export TF_VAR_client_secret=<service_principal_password>
export TF_VAR_client_id=<service_principal_appid>
export TF_VAR_subcription_id=<your_subscription_id>
export TF_VAR_tenant_id=<your_tenant_id>
export TF_VAR_gitlab_token=<token>
```

## Backend

As a backend for storing Terraform state files I have chosen Gitlab for easier start.

## Terraform file structure

```
terraform/
|-- modules/
|   |-- aks/                    # aks module contains Azure Kubernetes resources
|   |-- flux/                   # flux module as gitops solution for managing kubernetes resources
|   |-- network/                # network module contains Azure network resources
|   |-- resource_group/         # resource_group module contains azure resource group resource
|-- main.tf                     # declarations of modules
|-- variables.tf    
|-- providers.tf    
|-- terraform.tfvars            # there sits a values 
```

## TODOs

- add modue for gitops solution (fluxcd)
- add acr module for storing images and helm packages
- add blob module where logs file can be stored

