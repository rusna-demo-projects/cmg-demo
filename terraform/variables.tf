variable "resource_group_name" {
  type        = string
  description = "Name of the resource group to create"
}

variable "prefix" {
  type        = string
  description = "Prefix to use for all resources"
}

variable "location" {
  type        = string
  description = "Location of resources"
  default     = "West Europe"
}

variable "acr_name" {
  type        = string
  description = "The name of the Azure Container Registry to create."
}

variable "acr_sku" {
  type        = string
  description = "The SKU of the Azure Container Registry to create."
}

variable "k8s_cluster_name" {
  type        = string
  description = "Name of the AKS cluster"
}

variable "k8s_cluster_node_pool" {
  type        = string
  description = "Name of the AKS cluster node pool"
}

variable "k8s_version" {
  type        = string
  description = "Version of Kubernetes"
}

variable "node_count" {
  type        = number
  description = "Number of nodes in the Kubernetes cluster"
}

variable "node_size" {
  type        = string
  description = "Size of the Kubernetes nodes"
}

variable "vnet_cidr" {
  type        = string
  description = "CIDR block for the VNet"
}

variable "subnet_cidr" {
  type        = string
  description = "CIDR block for the subnet"
}

variable "client_id" {
  type        = string
  description = "The ID of the service principal that you created in Azure."
}

variable "client_secret" {
  type        = string
  description = "The secret key for the service principal."
}

variable "subscription_id" {
  type        = string
  description = "The Azure subscription ID represents your subscription in Azure."
}

variable "tenant_id" {
  type        = string
  description = "The Azure tenant ID is a unique identifier assigned to your Azure Active Directory (Azure AD) instance."
}