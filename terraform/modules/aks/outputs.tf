output "kube_config" {
  value = azurerm_kubernetes_cluster.this.kube_config_raw
}

output "kube_config_path" {
  value = "${path.module}/kubeconfig_${var.k8s_cluster_name}.yaml"
}
