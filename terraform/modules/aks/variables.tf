variable "prefix" {
  type        = string
  description = "Prefix to use for all resources"
}

variable "location" {
  type        = string
  description = "Location of resources"
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group in which to create the Azure Container Registry."
}

variable "node_count" {
  type        = number
  description = "Number of nodes in the default pool"
}

variable "node_size" {
  type        = string
  description = "VM size for the default pool"
}

variable "k8s_cluster_name" {
  type        = string
  description = "Name of the AKS cluster"
}

variable "k8s_cluster_node_pool" {
  type        = string
  description = "Name of the AKS cluster node pool"
}

variable "k8s_version" {
  type        = string
  description = "Kubernetes version to use for the cluster"
}

variable "vnet_name" {
  type        = string
  description = "Name of the virtual network"
}

variable "subnet_id" {
  type        = string
  description = "Id of the subnet"
}

variable "client_id" {
  type        = string
  description = "The ID of the service principal that you created in Azure."
}

variable "client_secret" {
  type        = string
  description = "The secret key for the service principal."
}
