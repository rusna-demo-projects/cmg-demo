resource "azurerm_kubernetes_cluster" "this" {
  name                = var.k8s_cluster_name
  location            = var.location
  resource_group_name = var.resource_group_name
  dns_prefix          = "${var.prefix}-dns"
  kubernetes_version  = var.k8s_version

  default_node_pool {
    name            = var.k8s_cluster_node_pool
    node_count      = var.node_count
    vm_size         = var.node_size
    vnet_subnet_id  = var.subnet_id
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }

  tags = {
    environment = var.prefix
  }
}