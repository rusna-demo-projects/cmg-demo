variable "location" {
  type        = string
  description = "The location of the resource group in which to create the Azure Container Registry."
}

variable "acr_name" {
  type        = string
  description = "The name of the Azure Container Registry to create."
}

variable "acr_sku" {
  type        = string
  description = "The SKU of the Azure Container Registry to create."
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group in which to create the Azure Container Registry."
}