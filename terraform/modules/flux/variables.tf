variable "gitlab_token" {
  description = "gitlab token"
  type        = string
  sensitive   = true
}

variable "repository_name" {
  description = "gitlab repository name"
  type        = string
  default     = "test-provider"
}

variable "repository_visibility" {
  description = "how visible is the gitlab repo"
  type        = string
  default     = "private"
}

variable "branch" {
  description = "branch name"
  type        = string
  default     = "master"
}

variable "target_path" {
  description = "flux sync target path"
  type        = string
}