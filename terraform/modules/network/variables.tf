variable "prefix" {
  type        = string
  description = "Prefix to use for all resources"
}

variable "location" {
  type        = string
  description = "Location of resources"
}

variable "resource_group_name" {
  type        = string
  description = "The name of the resource group in which to create the Azure Container Registry."
}

variable "vnet_cidr" {
  type        = string
  description = "CIDR block for the virtual network"
}

variable "subnet_cidr" {
  type        = string
  description = "CIDR block for the subnet"
}
