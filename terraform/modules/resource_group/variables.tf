variable "location" {
  type        = string
  description = "Location of the resource group."
}

variable "resource_group_name" {
  type        = string
  description = "Name of the resource group to create"
}

variable "prefix" {
  type        = string
  description = "Prefix to use for all resources"
}