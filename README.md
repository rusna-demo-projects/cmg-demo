# cmg-demo

Interview demo project for CMG company.

Project should demonstrate the ability to design infrastructure, implement particular services, and provision infrastructure where services could run.

## File structure

Repository contains **terraform** folder for provisioning azure infrastructure, and **services** folder where sits python apps.

### Repository overview
- `charts` - where helm chart and k8s templates sits
- `log_processor` - python applications [README](log_processor/README.md)
- `terraform` - provisioning of azure aks, acr [README](terraform/README.md)
- `gitlab-ci.yaml` - CI/CD definition

## GitLab
### CI/CD stages

- prepare
- validate
- build
- deploy
- cleanup

* `prepare`  - do **terraform  init**
* `validate` - validation of configuration files (terraform)
* `build`    - do **terraform plan** and **plan-json**
* `deploy`   - do **terraform apply**
* `cleanup`  - do **terraform destroy**

## TODOs

- add missing jobs for building and publishing images
- add missing jobs for packaging and publish helm chart

## TODOs for operational project

- docker build and push jobs for log_processor and log_controller
- helm package and publish jobs
- create controller app for orchestrating log lifecycle
  - deployment k8s template 
- finish terraform provisioning

## TODOs for desired state

- an infrastructure diagram
