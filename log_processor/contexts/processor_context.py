from log_processor.processors.processor import Processor
from log_processor.processors.thermometer_processor import ThermometerProcessor
from log_processor.processors.humidity_processor import HumidityProcessor


class ProcessorContext:
    _instance = None

    def __new__(cls, processor):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._instance.context = processor
        return cls._instance

    @classmethod
    def empty(cls):
        return cls._instance is None

    @classmethod
    def context(cls) -> Processor:
        return cls._instance.context

    @classmethod
    def sensor_name(cls) -> str:
        processor = cls.context()
        # sadly, this need to be like that, bc I haven't found way how to work with abstract properties
        # I'm was getting 'RecursionError: maximum recursion depth exceeded' on getter
        if isinstance(processor, HumidityProcessor):
            return processor.sensor_name
        elif isinstance(processor, ThermometerProcessor):
            return processor.sensor_name

    @classmethod
    def process_value(cls, new_value):
        processor = cls.context()
        if isinstance(processor, HumidityProcessor):
            processor.update_mean(new_value)
        elif isinstance(processor, ThermometerProcessor):
            processor.update_running_mean_var(new_value)

    @classmethod
    def sensor_brand(cls):
        return cls.context().brand()

    @classmethod
    def new(cls, processor):
        ProcessorContext.reset()
        processor_context = ProcessorContext(processor)
        return processor_context

    @classmethod
    def reset(cls):
        cls._instance = None


