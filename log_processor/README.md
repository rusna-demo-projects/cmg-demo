Python apps for processing sensor logs. 

If you want to run it from localhost do the `export PYTHONPATH=$PYTHONPATH:~/log_processor` from parent cmg-demo directory.
It will fix problem with `ModuleNotFoundError: No module named 'log_processor'`.

Then you can run it by `python3 log_processor`.

To build docker image:

```
docker build -t log_processor .
```

## TODOs

- log_processor
  - Add logging library
  - Access log path from env variable
  - Add azure library for accessing blob-storage
  - create json output file

- log_controller 
  - service which will spawn log_processor jobs when new log file appears on blob storage.
