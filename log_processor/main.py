import os
import re
from collections import defaultdict
from log_processor.contexts.processor_context import ProcessorContext
from log_processor.processors.thermometer_processor import ThermometerProcessor
from log_processor.processors.humidity_processor import HumidityProcessor

TIMESTAMP_FORMAT = re.compile(r'\d{4}-\d{2}-\d{2}T\d{2}:\d{2}')


def save_sensor_result(data):
    if not ProcessorContext.empty():
        if ProcessorContext.sensor_name().__contains__("temp"):
            data["thermometers"][ProcessorContext.sensor_name()] = ProcessorContext.sensor_brand()
        elif ProcessorContext.sensor_name().__contains__("hum"):
            data["humidity_sensors"][ProcessorContext.sensor_name()] = ProcessorContext.sensor_brand()


def read_log_file(file_path):
    with open(os.path.join(os.path.dirname(__file__), file_path), 'r') as log_file:
        for line in log_file:
            yield line.strip()


def main():
    log_file_path = "sensor_data.log"

    data = {
        "reference": {},
        "thermometers": defaultdict(list),
        "humidity_sensors": defaultdict(list)
    }

    for line in read_log_file(log_file_path):

        if line.startswith("reference"):
            _, temperature, humidity = line.split()
            data["reference"]["temperature"] = float(temperature)
            data["reference"]["humidity"] = float(humidity)

        elif line.startswith("thermometer"):
            _, sensor_name = line.split()
            save_sensor_result(data)
            ProcessorContext.new(ThermometerProcessor(sensor_name, data["reference"]["temperature"]))

        elif line.startswith("humidity"):
            _, sensor_name = line.split()
            save_sensor_result(data)
            ProcessorContext.new(HumidityProcessor(sensor_name, data["reference"]["humidity"]))

        else:
            timestamp, value = line.split()
            if TIMESTAMP_FORMAT.match(timestamp):
                ProcessorContext.process_value(float(value))
            else:
                print("Wrong timestamp format")

    # Save results last time from actual Processor context
    save_sensor_result(data)

    print(data)
