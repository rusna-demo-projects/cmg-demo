from log_processor.processors.processor import Processor
from math import fabs


class HumidityProcessor(Processor):

    def __init__(self, name, humidity):
        self.sensor_name = name
        self.entry_count = 0
        self.current_mean = 0.0
        self.reference_humidity = humidity

    def brand(self):
        if fabs(self.current_mean - self.reference_humidity) > 1:
            return "discard"
        else:
            return "keep"

    def update_mean(self, new_value):
        # used algorithm is running average
        self.entry_count += 1
        delta = new_value - self.current_mean
        self.current_mean += delta / self.entry_count

