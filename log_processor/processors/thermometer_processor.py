from log_processor.processors.processor import Processor
from math import fabs, sqrt


class ThermometerProcessor(Processor):

    def __init__(self, name, temperature):
        self.sensor_name = name
        self.entry_count = 0
        self.current_mean = 0.0
        self.reference_temperature = temperature
        self.current_variance = 0.0

    def brand(self):
        if fabs(self.current_mean - self.reference_temperature) <= 0.5 and self.standard_deviation() <= 3:
            return "ultra precise"
        elif fabs(self.current_mean - self.reference_temperature) <= 0.5 and self.standard_deviation() <= 5:
            return "very precise"
        else:
            return "precise"

    def update_running_mean_var(self, new_value):
        self.entry_count += 1
        delta = new_value - self.current_mean
        self.current_mean += delta / self.entry_count
        delta2 = new_value - self.current_mean
        self.current_variance += (delta * delta2) / self.entry_count

    def standard_deviation(self):
        return sqrt(self.current_variance)
