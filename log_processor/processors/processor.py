from abc import ABC, abstractmethod


class Processor(ABC):

    @abstractmethod
    def brand(self):
        pass
